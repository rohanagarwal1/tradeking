Streaming App using TradeKing
===

First start by creating an account at tradeking.com. Hit me up for a referral so we both get $50.

This application uses the Streaming API, explained here: https://developers.tradeking.com/documentation/java-streaming

Before building the project, create a directory `resources` under `src/main` and place `config.properties` there.

The properties file should contain the following keys:
CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET

From the root of the project, run `gradle build`. This will produce a fatjar under `build/libs` called `streaming-app.jar`.

Run using `java -jar build/libs/streaming-app.jar TWTR` or any other Stock Ticker symbol. You can only pass one argument. 

package streaming;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.jetty.JettyOAuthConsumer;

import org.mortbay.io.Buffer;
import org.mortbay.jetty.client.ContentExchange;
import org.mortbay.jetty.client.HttpClient;

public class Streaming {

  public static void main(String[] args) throws Exception {
    if (args.length != 1) {
      throw new IllegalArgumentException();
    }
    new Streaming().run(args[0]);
  }

  public void run(String ticker) throws Exception {
    // load properties
    Properties prop = new Properties();
    InputStream input = this.getClass().getClassLoader().getResourceAsStream("config.properties");
    prop.load(input);

    String CONSUMER_KEY = prop.getProperty("CONSUMER_KEY");
    String CONSUMER_SECRET = prop.getProperty("CONSUMER_SECRET");
    String ACCESS_TOKEN = prop.getProperty("ACCESS_TOKEN");
    String ACCESS_TOKEN_SECRET = prop.getProperty("ACCESS_TOKEN_SECRET");

    // create a consumer object and configure it with the access
    // token and token secret obtained from the service provider
    OAuthConsumer consumer = new JettyOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
    consumer.setTokenWithSecret(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

    // create an HTTP request to a protected resource
    ContentExchange request = new ContentExchange(true) {
      // tell me what kind of response code we got
      protected void onResponseComplete() throws IOException {
        int status = getResponseStatus();
        if (status == 200)
          System.out.println("Successfully connected");
        else
          System.out.println("Error Code Received: " + status);
        }

      // print out any response data we get along the stream
      protected void onResponseContent(Buffer data) {
        System.out.println(data);
      }
    };

    // setup the request
    request.setMethod("GET");
    String url = "https://stream.tradeking.com/v1/market/quotes.json?symbols=" + ticker;
    System.out.println("Setting URL to: " + url);
    request.setURL(url);

    // sign the request
    consumer.sign(request);

    // send the request
    HttpClient client = new HttpClient();
    client.start();
    client.send(request);
    request.waitForDone();
  }
}